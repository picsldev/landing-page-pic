"use strict";

// Carga de la pantalla inicial
$(document).ready(function () {
  $(".header").height($(window).height());
});

$(".navbar a").click(function () {
  $("body,html").animate(
    {
      scrollTop: $("#" + $(this).data("value")).offset().top,
    },
    1000
  );
});

// https: //stackoverflow.com/questions/23976498/fading-bootstrap-navbar-on-scrolldown-while-changing-text-color#
// $(window).scroll(function () {
//    if ($(this).scrollTop() > 300) {
//        $('.navbar-fixed-top').addClass('opaque');
//    } else {
//        $('.navbar-fixed-top').removeClass('opaque');
//    }
// });

window.addEventListener(
  "scroll",
  function () {
    if (window.scrollY > 150) {
      $(".navbar").fadeOut();
    } else {
      $(".navbar").fadeIn();
    }
  },
  false
);

$(document).ready(function () {
  var year = new Date().getFullYear();
  $("#fecha").text(year);
});

//function suma(a, b) {
//  // var sum = a + b;
//  var suma = new Date().getFullYear();
//  document.getElementById("resultado").innerHTML = sum;
//}

//function doAmazingThings() {
//  var year = new Date().getFullYear();
//  $("#fecha").text(year);
//  // alert('YOU AM AMAZING!');
//}
//document.addEventListener('DOMContentReady', function () {
//  document.getElementById('amazing')addEventListener('click', doAmazingThings);
//});
//document.getElementById("fecha").innerHTML = doAmazingThings();

// EOF
